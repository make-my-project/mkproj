package initialize

import (
	"gitlab.com/make-my-project/mkproj/src/help"
	"github.com/spf13/pflag"
)

// defining flags for the program
var (

	// for identifying the specified FileName
	FileName string
)

func Init() string {

	// defining flags
	pflag.StringVarP(&FileName, "file", "f", "", help.FileNameHelp)

	//parsing the flags
	pflag.Parse()

	//returning the value entered via the flag
	return FileName
}
