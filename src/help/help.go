
package help

var FileNameHelp string = `: The name of the file/project that need to be created with the extension
: Example:  dev -f main.py
	      dev -f app.js
	      dev -f project_name.html5
				def -f ls (show all the supported formats)
`

var SupportedFormats string = `: Supported Formats
	for creating an angular-cli project:
		project_name.angular-cli OR
		project_name.angularCLI

	for creating a angular-js project:
		project_name.angular-js OR
		project_name.angularJS

	for creating a bash file:
		file_name.sh

	for creating a bootstrap project:
		project_name.bootstrap OR
		project_name.bstrp

	for creating a express.js project:
		project_name.express OR
		project_name.expressJS OR
		project_name.expressjs OR
		project_name.express-js

	for creating a go project or a file (depends on the avalability of the go.mod file in the dir)
		project_name.go OR file_name.go

	for creating a html file (will create css/ and js/ if the file_name is index.html)
		file_name.html (contains a basic web boiler plate for index.html)

	for creating a html5 web boiler plate:
		project_name.html5

	for creating python3 and python2 files respectively:
		file_name.py3 and file_name.py2

	for creating a react project:
		project_name.r OR
		project_name.react

	for creating a react-native project:
		project_name.react-native OR
		project_name.rn OR
		project_name.RN OR
		peroject_name.reactNative

	for creating a typescript file:
		file_name.ts OR
		file_name-tcs

	for any other file:
		file_name.extension
`

