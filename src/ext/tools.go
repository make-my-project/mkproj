package ext

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"gitlab.com/make-my-project/mkproj/pkg/export"
	"gitlab.com/make-my-project/mkproj/pkg/install"
	isinstalled "gitlab.com/make-my-project/mkproj/pkg/is_installed"
)


//// This Page contains common functions that are used repeatedly by other local
//packages

// Getting the current working directory of the user
func GetCwd() string {

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	return dir

}

// Installing the pacapt universal package manager
func pacapt() {

	// Creating a tmp file
	// Storing the pacapt script in the tmp file
	// making the tmp file executable
	// Running the pacapt script

	tmpFile, err := ioutil.TempFile(GetCwd(), "PacaptInstallScript")
	if err != nil {
		panic(err)
	}

	defer os.Remove(tmpFile.Name())

	tmpFile.WriteString(install.InstallPacaptScript)
	tmpFile.Close()
	os.Chmod(tmpFile.Name(), 0755)

	cmd := exec.Command("bash", tmpFile.Name())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		os.Remove(tmpFile.Name())
		panic(err)
	}


}


// Install packages using pacapt (universal installer)
func Installer(pkg string) {

	// Installing pacapt
	pacapt()

	// using the pacman (Don`t worry if you are using arch then this will only
	// use native pacman for other systems this will use pacapt by syslinking it
	// to pacman)

	cmd := exec.Command("sudo", "pacman", "-S", "--no-confirm", pkg)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		panic(err)
	}

	fmt.Printf("The package %v has been installed", pkg)

}


// adding npm to path
func SetPrefixNPM() {

	// checking wether npm is installled
	cmd := exec.Command("npm", "--version")

	if err := cmd.Run(); err != nil {
		fmt.Println("npm is not installed")
		fmt.Println("--------------------")
		fmt.Println("Installing npm")
		fmt.Println("--------------------")

		Installer("npm")

		fmt.Println("--------------------")
	}

	// creating a tmp file
	// adding the npm prefix script to th tmp file
	// making the tmp file executable
	// running the tmp file

	tmpFile, err := ioutil.TempFile(GetCwd(), "NPMToPATH")
	if err != nil {
		panic(err)
	}

	fmt.Println("setting and exporting the npm-prefix")

	defer os.Remove(tmpFile.Name())

	tmpFile.WriteString(export.NpmAddToPathScript)
	tmpFile.Close()
	os.Chmod(tmpFile.Name(), 0755)

	cmd = exec.Command("bash", tmpFile.Name())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		os.Remove(tmpFile.Name())
		panic(err)
	}


}


// Installing nodejs
func SetPrefixNode() {

	// checking wether nodes is installed
	cmd := exec.Command("node", "--version")

	if err := cmd.Run(); err != nil{
		fmt.Println("nodejs is not installed")
		fmt.Println("--------------------")
		fmt.Println("Installing nodejs")
		fmt.Println("--------------------")

		Installer("nodejs")

		fmt.Println("--------------------")
	}

}


// Installing live-server
func InstallLiveServer() {

	// checking wether live-server is installed
	isLiveServerInstalled := true

	cmd := exec.Command("live-server", "--version")

	if err := cmd.Run(); err != nil {
		isLiveServerInstalled = false
	}

	if !isLiveServerInstalled {

		// creating a tmp file
		// adding the live-server check script to th tmp file
		// making the tmp file executable
		// running the tmp file

		tmpFile, err := ioutil.TempFile(GetCwd(), "LiveServerInstallCheckScript")
		if err != nil {
			panic(err)
		}

		defer os.Remove(tmpFile.Name())

		tmpFile.WriteString(isinstalled.IsLiveServerInstalled)
		tmpFile.Close()

		os.Chmod(tmpFile.Name(), 0755)

		cmd = exec.Command("bash", tmpFile.Name())

		if err := cmd.Run(); err != nil {
			os.Remove(tmpFile.Name())
			isLiveServerInstalled = false
		}

	}

	if !isLiveServerInstalled {

		// creating a tmp file
		// adding the live-server script to th tmp file
		// making the tmp file executable
		// running the tmp file

		tmpFile, err := ioutil.TempFile(GetCwd(), "LiveServerInstallScript")
		if err != nil {
			panic(err)
		}

		defer os.Remove(tmpFile.Name())

		tmpFile.WriteString(install.LiveServerInstallScript)
		tmpFile.Close()
		os.Chmod(tmpFile.Name(), 0755)

		cmd = exec.Command("bash", tmpFile.Name())
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {

			SetPrefixNPM()

			cmd = exec.Command("bash", tmpFile.Name())
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			if cmd.Run() != nil {
				os.Remove(tmpFile.Name())
				panic(cmd.Run())
			}


		}

	}

}


// Installing create-react-app
func InstallCreateReactApp() {

	// checking wether live-server is installed
	isCreateReactAppInstalled := true

	cmd := exec.Command("create-react-app", "--version")

	if err := cmd.Run(); err != nil {
		isCreateReactAppInstalled = false
	}

	if !isCreateReactAppInstalled {

		// creating a tmp file
		// adding the live-server check script to th tmp file
		// making the tmp file executable
		// running the tmp file

		tmpFile, err := ioutil.TempFile(GetCwd(), "CreateReactAppInstallScript")
		if err != nil {
			panic(err)
		}

		defer os.Remove(tmpFile.Name())

		tmpFile.WriteString(isinstalled.IsCreateReactAppInstalled)
		tmpFile.Close()

		os.Chmod(tmpFile.Name(), 0755)

		cmd = exec.Command("bash", tmpFile.Name())

		if err := cmd.Run(); err != nil {
			os.Remove(tmpFile.Name())
			isCreateReactAppInstalled = false
		}

	}

	if !isCreateReactAppInstalled {

		// creating a tmp file
		// adding the live-server script to th tmp file
		// making the tmp file executable
		// running the tmp file

		tmpFile, err := ioutil.TempFile(GetCwd(), "CreateReactAppInstallScript")
		if err != nil {
			panic(err)
		}

		defer os.Remove(tmpFile.Name())

		tmpFile.WriteString(install.CreateReactAppInstallScript)
		tmpFile.Close()
		os.Chmod(tmpFile.Name(), 0755)

		cmd = exec.Command("bash", tmpFile.Name())
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {

			SetPrefixNPM()

			cmd = exec.Command("bash", tmpFile.Name())
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			if cmd.Run() != nil {
				os.Remove(tmpFile.Name())
				panic(cmd.Run())
			}


		}

	}

}


// Installing create-react-app
func InstallSass() {

	// checking wether live-server is installed
	isSassInstalled := true

	cmd := exec.Command("sass", "--version")

	if err := cmd.Run(); err != nil {
		isSassInstalled = false
	}

	if !isSassInstalled {

		// creating a tmp file
		// adding the live-server check script to th tmp file
		// making the tmp file executable
		// running the tmp file

		tmpFile, err := ioutil.TempFile(GetCwd(), "SassCheckInstallScript")
		if err != nil {
			panic(err)
		}

		defer os.Remove(tmpFile.Name())

		tmpFile.WriteString(isinstalled.IsSassInstalled)
		tmpFile.Close()

		os.Chmod(tmpFile.Name(), 0755)

		cmd = exec.Command("bash", tmpFile.Name())

		if err := cmd.Run(); err != nil {
			os.Remove(tmpFile.Name())
			isSassInstalled = false
		}

	}

	if !isSassInstalled {

		// creating a tmp file
		// adding the live-server script to th tmp file
		// making the tmp file executable
		// running the tmp file

		tmpFile, err := ioutil.TempFile(GetCwd(), "SassInstallScript")
		if err != nil {
			panic(err)
		}

		defer os.Remove(tmpFile.Name())

		tmpFile.WriteString(install.CreateReactAppInstallScript)
		tmpFile.Close()
		os.Chmod(tmpFile.Name(), 0755)

		cmd = exec.Command("bash", tmpFile.Name())
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {

			SetPrefixNPM()

			cmd = exec.Command("bash", tmpFile.Name())
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			if cmd.Run() != nil {
				os.Remove(tmpFile.Name())
				panic(cmd.Run())
			}


		}

	}

}


// Installing create-react-app
func InstallTypeScript() {

	// checking wether typescript is installed
	isTypeScriptInstalled := true

	cmd := exec.Command("tsc", "--version")

	if err := cmd.Run(); err != nil {
		isTypeScriptInstalled = false
	}

	if !isTypeScriptInstalled {

		// creating a tmp file
		// adding the live-server check script to th tmp file
		// making the tmp file executable
		// running the tmp file

		tmpFile, err := ioutil.TempFile(GetCwd(), "typeScriptCheckInstalledScript")
		if err != nil {
			panic(err)
		}

		defer os.Remove(tmpFile.Name())

		tmpFile.WriteString(isinstalled.IsTypescriptInstalled)
		tmpFile.Close()

		os.Chmod(tmpFile.Name(), 0755)

		cmd = exec.Command("bash", tmpFile.Name())

		if err := cmd.Run(); err != nil {
			os.Remove(tmpFile.Name())
			isTypeScriptInstalled = false
		}

	}

	if !isTypeScriptInstalled {

		// creating a tmp file
		// adding the live-server script to th tmp file
		// making the tmp file executable
		// running the tmp file

		tmpFile, err := ioutil.TempFile(GetCwd(), "TypeScriptInstallScript")
		if err != nil {
			panic(err)
		}

		defer os.Remove(tmpFile.Name())

		tmpFile.WriteString(install.CreateReactAppInstallScript)
		tmpFile.Close()
		os.Chmod(tmpFile.Name(), 0755)

		cmd = exec.Command("bash", tmpFile.Name())
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {

			SetPrefixNPM()

			cmd = exec.Command("bash", tmpFile.Name())
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			if cmd.Run() != nil {
				os.Remove(tmpFile.Name())
				panic(cmd.Run())
			}


		}

	}

}

