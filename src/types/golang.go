package types

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/make-my-project/mkproj/src/ext"
)

//Golang
func Golang(fileName string) {

	// checkign wether the go.mod file exsists in the current directory
	if _, err := os.Stat("go.mod"); os.IsNotExist(err) {

		// asking user for the path for the project name
		fmt.Println("Give the path for your go project name")
		fmt.Println("Example : github.com/username/projectname (This will be used for go mod init)")
		fmt.Print(" : ")

		var projectName string
		fmt.Scanln(&projectName)

		projectName = strings.TrimSpace(projectName)

		// creating the go.mod file
		cmd := exec.Command("go", "mod", "init", projectName)
		if err := cmd.Run(); err != nil {

			fmt.Println("go is not installed")
			fmt.Println("-------------------")
			fmt.Println("Installing go")
			fmt.Println("-------------------")

			ext.Installer("go")

			cmd := exec.Command("go", "mod", "init", projectName)
			if cmd.Run() != nil {
				fmt.Println("There was an error with the go pkg")
				fmt.Println("Please check : https://wiki.archlinux.org/title/Go", " for bug fixing")
				panic(cmd.Run())
			}

		}

		// initialising the project with the go.mod file with the given name
		fmt.Println("Select a template to initialise your go program")
		fmt.Println("1 = > Cobra")
		fmt.Println("2 = > Viper")
		fmt.Println("3 = > Do my own thing (default) ")
		fmt.Print("Enter the number corresponding to your choice : ")

		var choice string
		fmt.Scanln(&choice)

		switch choice {

			case "1":
				fmt.Println("Initialising Cobra project")
				// Downloading cobra project
				// go get github.com/spf13/cobra/cobra
				cmdDownload := exec.Command("go", "get", "github.com/spf13/cobra/cobra")

				// Installing the cobra project
				cmdInstall := exec.Command("go", "install", "github.com/spf13/cobra/cobra")

				// Cobra init
				cmdInit := exec.Command("cobra", "init", "--pkg-name", projectName)

				// Running the commands
				cmdDownload.Stdout = os.Stdout
				cmdDownload.Stderr = os.Stderr

				if err := cmdDownload.Run(); err != nil {
					panic(err)
				}

				cmdInstall.Stdout = os.Stdout
				cmdInstall.Stderr = os.Stderr

				if err := cmdInstall.Run(); err != nil {
					panic(err)
				}

				cmdInit.Stdout = os.Stdout
				cmdInit.Stderr = os.Stderr

				if err := cmdInit.Run(); err != nil {
					panic(err)
				}

				// create the user specified filename in the cmd dir of cobra
				os.Create("cmd/"+fileName)

			case "2":
				fmt.Println("Initialising Viper project")
				// Downloading Viper project
				// go get github.com/spf13/viper

				cmdDownload := exec.Command("go", "get", "github.com", "spf13", "viper")
				cmdDownload.Stdout = os.Stdout
				cmdDownload.Stderr = os.Stderr

				if err := cmdDownload.Run(); err != nil {
					panic(err)
				}

			default:
		}

	}else {

		// create the given golang file
		os.Create(fileName)

	}

}

