package types

import (
	"os"

	"gitlab.com/make-my-project/mkproj/src/ext"
)

//typescript
func TypeScript(fileName string) {

	//check install typescript
	ext.InstallTypeScript()

	// create the typescript file
	os.Create(fileName)

}


