package types

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/make-my-project/mkproj/pkg/screen"
	"gitlab.com/make-my-project/mkproj/src/ext"
)

//sass
func Sass(fileName string) {

	// seperate the fileName and the ext
	fileName = strings.Split(fileName, ".")[0]
	fileName = fileName + ".scss"

	// check wether a sass folder exsits
	if _, err := os.Stat("sass"); os.IsNotExist(err) {
		// create sass folder
		os.Mkdir("sass", 0777)
	} else {
		// create the required file inside the sass folder that is exsisting
		os.Create("sass/" + fileName)
	}
	if _, err := os.Stat("css"); os.IsNotExist(err) {
		// create sass folder
		os.Mkdir("css", 0777)
	}

	// check install sass
	ext.InstallSass()

	// check and monitor the sass folder via a screen script

	// create a tmp file
	// add the sass screen script to the tmp file
	// make the tmp file executable
	// run the tmp file

	tmpFile, err := ioutil.TempFile(ext.GetCwd(), "SassScreenScript")
	if err != nil {
		panic(err)
	}

	defer os.Remove(tmpFile.Name())

	tmpFile.WriteString(screen.SassScreenScript)
	tmpFile.Close()

	os.Chmod(tmpFile.Name(), 0777)

	cmd := exec.Command("bash", tmpFile.Name())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		fmt.Println("installing screen")
		fmt.Println("-----------------")
		fmt.Println("installing screen")
		fmt.Println("-----------------")

		ext.Installer("screen")

		cmd := exec.Command("bash", tmpFile.Name())
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if cmd.Run() != nil {
			os.Remove(tmpFile.Name())
			panic(cmd.Run())
		}


	}

}
