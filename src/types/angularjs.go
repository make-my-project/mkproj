package types

import (
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/make-my-project/mkproj/pkg/install"
	"gitlab.com/make-my-project/mkproj/pkg/zipped"
	"gitlab.com/make-my-project/mkproj/src/ext"
)

func AngularJS(projectName string) {

	// seperating projectName and angularjs ext
	projectName = strings.Split(projectName, ".")[0]

	// creating the anhgularjs project
	zipped.Unzip("angularJS.zip", projectName)

	// change the dir to the project
	os.Chdir(projectName)

	// installing the npm modules in the package.json

	// Creating a tmp file
	// adding the nppm install scrip to the tmp file
	// making the tmp file executable
	// running the tmp file

	tmpFile, err := ioutil.TempFile(ext.GetCwd(), "NPMInstallScript")
	if err != nil {
		panic(err)
	}
	defer os.Remove(tmpFile.Name()) // clean up

	tmpFile.WriteString(install.NpmInstallScript)
	tmpFile.Close()

	os.Chmod(tmpFile.Name(), 0755) // make executable
	cmd := exec.Command("bash", tmpFile.Name())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if cmd.Run() != nil {
		ext.SetPrefixNPM()
		cmd := exec.Command("bash", tmpFile.Name())
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if cmd.Run() != nil {
			os.Remove(tmpFile.Name())
			panic(cmd.Run())
		}

	}


}

