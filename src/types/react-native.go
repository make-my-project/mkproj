package types

import (
	"os"
	"os/exec"
	"strings"

	"gitlab.com/make-my-project/mkproj/src/ext"
)

//react-native

func ReactNative(projectName string) {

	// seperate projectName and the ext
	projectName = strings.Split(projectName, ".")[0]

	// initialize react-native
	// /usr/bin/node /usr/lib/node_modules/npm/bin/npx-cli.js --yes --package react-native-cli react-native init untitled
	cmd := exec.Command("/usr/bin/node", "/usr/lib/node_modules/npm/bin/npx-cli.js", "--yes", "--package", "react-native-cli", "react-native", "init", projectName)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		ext.SetPrefixNode()

		cmd := exec.Command("/usr/bin/node", "/usr/lib/node_modules/npm/bin/npx-cli.js", "--yes", "--package", "react-native-cli", "react-native", "init", projectName)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if cmd.Run() != nil {
			ext.SetPrefixNPM()

			cmd := exec.Command("/usr/bin/node", "/usr/lib/node_modules/npm/bin/npx-cli.js", "--yes", "--package", "react-native-cli", "react-native", "init", projectName)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			if cmd.Run() != nil {
				panic(cmd.Run())
			}

		}

	}


}

