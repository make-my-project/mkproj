package types

import (
	"os"
	"os/exec"
	"strings"

	"gitlab.com/make-my-project/mkproj/src/ext"
)

//python3
func checkPython3() bool {

	cmd := exec.Command("python3", "--version")
	if err := cmd.Run(); err != nil {
		return false
	}

	return true

}

func installPython3() {

	if !checkPython3() {
		ext.Installer("python3")
	}

}

//grtting the python3 binary path (with relative version)
func getPy3() string {

	cmd := exec.Command("python3", "--version")
	output, err := cmd.Output()
	if err != nil {
		panic(err)
	}

	versionArray := strings.Split(strings.Split(string(output), " ")[1], ".")
	relativeVersion := versionArray[0] + "." + versionArray[1]

	return ("#!/bin/python" + relativeVersion + "\n")

}

func Python3(fileName string) {

	// check and install Python3
	installPython3()

	// Creating the python3 file
	// Adding the executable path
	// making it executable

	// seperating the filename and ext
	fileName = strings.Split(fileName, ".")[0]
	fileName = fileName + ".py"

	os.Create(fileName)
	file, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	file.WriteString(getPy3())
	os.Chmod(fileName, 0755)

}
