package types

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/make-my-project/mkproj/pkg/install"
	"gitlab.com/make-my-project/mkproj/src/ext"
)

// choosing the view Engine
func viewEngine() string {

	// printing the available options
	fmt.Println("Chose a view Engine ( the default is pug)")
	fmt.Println("1 = > pug(jade)")
	fmt.Println("2 = > Dust")
	fmt.Println("3 = > EJS")
	fmt.Println("4 = > Handlebars")
	fmt.Println("5 = > Jade")
	fmt.Println("6 = > JSP")
	fmt.Println("7 = > Hogan.js")
	fmt.Println("8 = > Twig")
	fmt.Println("9 = > none")
	fmt.Print("Enter the number corresponding to your choice : ")

	var userChoice string
	fmt.Scanln(&userChoice)

	return userChoice

}

// choosing a Stylesheet Engine
func styleSheetEngine() string {

// printing the available options
	fmt.Println("Chose a Stylesheet Engine ( the default is css)")
	fmt.Println("1 = > Plain CSS")
	fmt.Println("2 = > Stylus")
	fmt.Println("3 = > LESS")
	fmt.Println("4 = > Compass")
	fmt.Println("5 = > SASS")
	fmt.Print("Enter the number corresponding to your choice : ")

	var userChoice string
	fmt.Scanln(&userChoice)

	return userChoice

}

// check if node is installed if not install it
func nodeJS() {

	cmd := exec.Command("node", "--version")

	if err := cmd.Run(); err != nil {
		fmt.Println("nodejs is not installed")
		fmt.Println("-----------------------")
		fmt.Println("installing nodejs")
		fmt.Println("-----------------------")

		ext.Installer("nodejs")

	}

}


// express.js
func ExpressJS(projectName string) {

	// check if node is installed if not install it
	ext.SetPrefixNode()

	// sperating the project name from the ext
	projectName = strings.Split(projectName, ".")[0]

	// creating the project folder
	os.Mkdir(projectName, 0777)

	// change the directory to the project folder
	os.Chdir(projectName)

	// checking the user prefered view engine
	var defaultViewEngine string
	switch defaultViewEngine = viewEngine(); defaultViewEngine {
		case "1":
			defaultViewEngine = "pug"
		case "2":
			defaultViewEngine = "dust"
		case "3":
			defaultViewEngine = "ejs"
		case "4":
			defaultViewEngine = "handlebars"
		case "5":
			defaultViewEngine = "jade"
		case "6":
			defaultViewEngine = "jsp"
		case "7":
			defaultViewEngine = "hogan"
		case "8":
			defaultViewEngine = "twig"
		case "9":
			defaultViewEngine = "none"

		default:
			defaultViewEngine = "pug"
	}

	var defaultStyleSheetEngine string
	switch defaultStyleSheetEngine = styleSheetEngine(); defaultStyleSheetEngine {
		case "1":
			defaultStyleSheetEngine = "css"
		case "2":
			defaultStyleSheetEngine = "stylus"
		case "3":
			defaultStyleSheetEngine = "less"
		case "4":
			defaultStyleSheetEngine = "compass"
		case "5":
			defaultStyleSheetEngine = "sass"

		default:
			defaultStyleSheetEngine = "css"
		}


	// Creating the express.js project
	// /usr/bin/node /usr/lib/node_modules/npm/bin/npx-cli.js --yes --package express-generator express --force --css defaultViewEngine --view defaultViewEngine
	cmd := exec.Command("/usr/bin/node", "/usr/lib/node_modules/npm/bin/npx-cli.js", "--yes", "--package", "express-generator", "express", "--force", "--css", defaultStyleSheetEngine, "--view", defaultViewEngine)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		ext.SetPrefixNPM()
		cmd := exec.Command("/usr/bin/node", "/usr/lib/node_modules/npm/bin/npx-cli.js", "--yes", "--package", "express-generator", "express", "--force", "--css", defaultStyleSheetEngine, "--view", defaultViewEngine)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if cmd.Run() != nil {
			panic(cmd.Run())
		}

	}

	// Install all the dependencies in the package.json in the project folder

	// creating a tmp file
	// storing the npm install script in the tmp file
	// making the tmp file executable
	// running the tmp file

	tmpFile, err := ioutil.TempFile(ext.GetCwd(), "NPMInstallScript")
	if err != nil {
		panic(err)
	}

	defer os.Remove(tmpFile.Name())

	tmpFile.WriteString(install.NpmInstallScript)
	tmpFile.Close()

	os.Chmod(tmpFile.Name(), 0777)

	cmd = exec.Command("bash", tmpFile.Name())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if cmd.Run() != nil {
		panic(cmd.Run())
	}

}

