package zipped

import (
	"embed"
	"os"
	"os/exec"
	"strings"
)

//go:embed angularJS.zip html5.zip BootStrap.zip
var zipper embed.FS

// recreate the embeded zip file in the current directory
func Unzip(zipName, finalName string) {

	// recreating the zip file in the working directory
	data, _ := zipper.ReadFile(zipName)

	// Creating the given zp file
	zipFile, err := os.Create(zipName)
	if err != nil {
		panic(err)
	}

	// Writing the data in to the created zipFile
	zipFile.Write(data)
	zipFile.Close() // closing the opned zipFile

	// unzipping the zip file
	cmd := exec.Command("unzip", "-o", zipName)

	if err := cmd.Run(); err != nil {
		panic(err)
	}

	// removing the unwanted zip file
	os.Remove(zipName)

	// getting the extractedName from the zipName
	extractedName := strings.Split(zipName, ".")[0]

	// renaming the unzipped file to the user desired name
	os.Rename(extractedName, finalName)

}
