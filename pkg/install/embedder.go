
package install

import _ "embed"

//go:embed "pacapt.sh"
var InstallPacaptScript string

//go:embed "npm.sh"
var NpmInstallScript string

//go"embed" "sass.sh"
var SassInstallScript string

//go:embed "typescript.sh"
var TypescriptInstallScript string

//go:embed "live-server.sh"
var LiveServerInstallScript string

//go:embed "angularcli.sh"
var AngularCliInstallScript string

//go:embed "create-react-app.sh"
var CreateReactAppInstallScript string
